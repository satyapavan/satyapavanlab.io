---
title: "Projects"
date: 2018-12-11T00:09:08+05:30
draft: false
---

{{< gallery caption-effect="fade" >}}
  {{< figure link="img/hexagon-thumb.jpg" caption="/about/projects/" >}}
  {{< figure thumb="-thumb" link="/img/sphere.jpg" caption="Sphere" >}}
  {{< figure thumb="-thumb" link="/img/triangle.jpg" caption="Triangle" alt="This is a long comment about a triangle" >}}
{{< /gallery >}}
