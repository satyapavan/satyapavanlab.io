---
title: "About"
date: 2018-12-10T22:55:50+05:30
draft: false
---
## I am Satya Pavan
---------------------

Holds a bachelors degree in **Electronics** and a masters degree in **Computers**.

A Software Engineer working as a **Solutions Architect** for a Telecom gaint.

**C++ and Java** developer by profession, **python**'er by passion and **javascript**'er by necessity.

I have a softspot for **\*nix shells** but my latest crush is with **Microsoft**.

> What is the most difficult part of being an architect? \
Well, it's not to code it all by yourself :-)
